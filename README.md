
# YUPLOTTER


This project is made to ease the mission planning and data processing of the YUCO AUV made by [Seaber](https://seaber.fr/)
![YUCO](https://seaber.fr/wp-content/uploads/2021/09/vignette-ctd.jpg)
|:--:| 
| *Fig 1. The µ-AUV YUCO* |

Yuplotter runs on *Jupyter*, is coded in *python 3.9* and is **Open-Source** 


## Working principle

### OpenCpn Symbiosis

YUPLOTTER works in combination with the Open-Source marine chartplotter [OpenCpn](https://opencpn.org/).

Running YUPLOTTER will exports *.gpx* files in the specific following directory :
```bash
MAC : /Users/jsmith/Library/opencpn/layers/
WINDOWS : C:/ProgamData/opencpn/layers/
```
This specific directory is read at the start-up of OpenCpn, and any *.gpx* file present will be displayed on the chart.

Those exports are useful, for mission planning in tandem with [Seaplan](https://seaber.fr/seaplan-software) and also for mission post-processing.

### Standalone scientific display

YUPLOTTER also allows the user to display directly in the *Jupyter Notebook* and save as *.html* or as *.png* some scientific 2D/3D plots with data previously downloaded from the YUCO.

### File organisation

In this Git repository several files can be found :

- YUPLOTTER.ipynb : The actual *Jupyter* Notebook that will be run

- YUPLOTTER_logic.py : Contains all the functions and folders paths needed to execute YUPLOTTER.ipynb 

- config_mac.txt : The configuration file if the user is running under macOS containing parameters such as your YUCO name and some file paths. This file is to be modified by each user.

- config_win.txt : The configuration file if the user is running under Windows.

Note : YUPLOTTER_logic.py is just imported at the top of YUPLOTTER.ipynb

## Configuration files

Inside the config file, 4 variables can be found and are required to be modified by the user :

```bash
opencpnLayerDir = /Users/hsellet/Library/Preferences/opencpn/layers/
opencpnPath = /Applications/OpenCPN.app/
cruisesDir = /Users/hsellet/Desktop/YUCO/Campagnes/
```
     
"opencpnLayerdir" is the path to the layers directory containing the *.gpx* files that is automatically opened by OpenCPN on startup. More information can be found [here](https://opencpn.org/wiki/dokuwiki/doku.php?id=opencpn:opencpn_user_manual:toolbar_buttons:route_mark_manager:layers#layer_directory_for_automatic_loading_of_permanent_layers)
     
"opencpnPath"  is the path to the openCPN app or executable depending on the OS. It allows to run OpenCpn on a button click in the *Jupyter Notebook*

"cruisesDir" is the path to the campaign directory containing all your AUV missions  *.json* and your data files *.csv*


## The Cruises directory

The *cruises* directory will contain a subdirectory created with Seaplan for each cruise.
It's advised to name each subdirectory as:
```bash
/year_month_day_locationOfCruise/
e.g. /2023_03_08_bayOfBiscay/
```
Inside the ```/year_month_day_locationOfCruise/```directory, several files will be found. Those files will be later imported by ```YUPLOTTER.ipynb```as a ```pandas.Dataframe``` and are organised as follow :

- ```MissionFile.json``` : by default, inside each *cruise* subdirectory, a *.json* file for each mission created via Seaplan. This file contains the mission instructions for the YUCO (waypoints, depths, bearing, distance to travel ...)

- ```points_of_interests.xsl``` : An optionnal excel file can be put with points of interests. Those points would be displayed in OpenCPN.
    The excel must be composed of a 'Name', 'Latitude_DD' (in Degrees Decimal) and 'Longitude_DD' columns as presented in Fig 2.

    ![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/points_of_interests_format.png) 
    |:--:| 
    | *Fig 2. points_of_interests.xls with embedded conversion from coordinates in Degrees decimal Minutes to Degrees Decimal* |

- ```Bathymetry.nc``` : An optionnal *netcdf* file can be put in order to display the bathymetry in the jupyter notebook for the scientific plots

- ```./export/myMissionData.csv```  : The export subdirectory is where all the data from the YUCO will be exported via Seaplan in the form of one ```.csv``` per realized mission. This ```.csv``` contains the navigation and scientific data of the YUCO. 

Note : A cruise folder with a netcdf bathymetry and points of interests is given as example for the user in the Gitlab for the user under ```/cruise_example/``` and would tipically look like Fig 3.

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/cruise_folder_organisation.png) 
|:--:| 
    | *Fig 3. Organisation of a cruise folder* |

## Installation

To run this project, an installed version of *python 3* is required

- Install *Jupyter* which can be installed with pip running the following command: 
    ```bash
    pip install jupyterlab
    ```

- Download and install OpenCpn ([download link](https://opencpn.org/OpenCPN/info/downloadopencpn.html))

-  Open config_mac.txt or config_win.txt depending on the OS and modify the 4 variables as explained in Configuration files

- Open *YUPLOTTER.ipynb* with *Jupyter* and run the cells. Make sur to download every library present under YUPLOTTER_logic.py


## Running the Jupyter

While running the *Jupyter* file a few specific cells require the following explanation for clarification

First, a path to the cruise folder to be analyzed will be asked. Select as in *Fig 4.* :

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/choosing_cruise_folder.png) 
|:--:| 
| *Fig 4. Choosing a cruise folder* |

Select the ```myMissionData.csv``` file to be analyzed in *Fig 5.* The corresponding ```MissionFile.json``` planned mission file used for the mission will automatically be linked.

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/select_mission_file.png) 
|:--:| 
| *Fig 5. Choosing a mission to analyze* |

In **1. OpenCPN Exports** it's possible to export AUV track from the previously selected ```myMissionData.csv``` as a ```.gpx``` to OpenCPN.

The whole campaign can also be exported as ```.gpx``` to OpenCPN (also in **1. OpenCPN Exports**).

Clicking on the yellow button "Delete OpenCPN Tracks" removes all the currently exported ```.gpx``` file from the ```/layers/``` directory of OpenCPN.

Clicking on the orange button "Restart OpenCPN" restarts openCPN.

**Important :** Once the ```.gpx``` are exported, OpenCPN needs to be restarted for the tracks to be shown on the chartplotter

See *Fig 6.*

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/opencpn_exports.png) 
|:--:| 
| *Fig 6. Exporting under openCPN* |

From this point on, all the plots in **2. Navigation** and **3. Science**  will be for the selected ```myMissionData.csv``` as shown in *Fig 7.*, *Fig 8.* and *Fig 9.*.

All displayed plots will be exported as ```.html``` in the ```myMissionData.csv``` parent folder.


![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/navigation_3D_plot.png) 
|:--:| 
| *Fig 7. 3D plot of the analyzed mission* |

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/CTD_plot.png) 
|:--:| 
| *Fig 8. 2D CTD plot of the analyzed mission* |

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/salinity_3D_plot.png) 
|:--:| 
| *Fig 9. 3D salinity plot of the analyzed mission* |

Under **5. Cruise** a few plots gathering the totality of the data of the ```/year_month_day_locationOfCruise/``` directory are displayed, and once displayed,  exported as ```.html```
under ```/year_month_day_locationOfCruise/exports/```

### In OpenCPN

The tracks, points of interests and mission files previously exported are now showed in OpenCPN (once OpenCPN is restarted) as shown in *Fig 10.*

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/opencpn_tracks.png) 
|:--:| 
| *Fig 10. Some tracks under OpenCPN* |

Each exported layer (each *.gpx* file) can be hidden or shown by going in *Route & Mark Manager* and clicking the "eye" icon as shown in *Fig 11.*

![image](https://gitlab.ifremer.fr/hs7abf8/yuplotter/-/raw/main/Readme_captures/opencpn_layers.png) 
|:--:| 
| *Fig 11. Route & Mark Manager* |

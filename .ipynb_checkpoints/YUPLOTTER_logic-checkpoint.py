#### CHANGEABLE VARIABLES IN config_XXX.txt file ####
import platform

opencpnLayerDir= ''
opencpnPath = ''
cruisesDir = ''


if platform.system() == 'Darwin': #For Mac
    with open('config_mac.txt') as f:
        lines = f.readlines()
        opencpnLayerDir = lines[0].split(' = ')[1].strip()
        opencpnPath = lines[1].split(' = ')[1].strip()
        cruisesDir = lines[2].split(' = ')[1].strip()
        #yucoNames = lines[3].split(' = ')[1].split(',')

if platform.system() == 'Windows': #For Windows
    with open('config_win.txt') as f:
        lines = f.readlines()
        opencpnLayerDir = lines[0].split(' = ')[1].strip()
        opencpnPath = lines[1].split(' = ')[1].strip()
        cruisesDir = lines[2].split(' = ')[1].strip()
        #yucoNames = lines[3].split(' = ')[1].strip().split(',')
        

## Specify here the type of bathymetry you are using. ATM, only 'MARS' and 'CROCO' bathymetry are supported
bathyType = 'MARS' #'CROCO'

#### IMPORTS ####

# Data processing libraries
import datetime
import json
import math
import netCDF4 as nc
import numpy as np
import pandas as pd

import gpxpy
import gpxpy.gpx
import gsw
from os import listdir
from geopy import distance
from geographiclib.geodesic import Geodesic

# GUI libraries
import ipywidgets as widgets
from ipylab import JupyterFrontEnd
from ipyfilechooser import FileChooser
import panel as pn

# Visualization libraries
import chart_studio.plotly as py
import cufflinks as cf
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER 
from cartopy.io.shapereader import Reader
import seaborn as sns
#%matplotlib inline
import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=False)

# System libraries
import os
import time
import IPython





#### FUNCTIONS DEFINITION ####


#Lis le nom des fichiers .json dans le dossier de campagne        
def missionReader(path) :      
    #Charge le dossier de mission
    mis = open(path)
    mis = json.load(mis)

    jsonMisName = mis['mission']['settings']['name']

    #nRadials = len(mis['mission']['steps'])-1

    # Un jour il faudrait dessiner les screws

    #Creating a dataframe from the navigation steps inside the json
    for key in mis['mission']['steps'] : 

        #gives the first value of the key (aka the step type)
        key1 = next(iter(key))

        if key1 == 'startingPoint' : 
            locDict = {'JsonName' : jsonMisName,
                       'Latitude' : key['startingPoint']['latitudeDeg'], 
                       'Longitude' : key['startingPoint']['longitudeDeg'],
                       'StepType' : 'startingPoint'
                       }
            dfSteps = pd.DataFrame(data = locDict, index = [0])

        if key1 == 'waypoint':
            locDict =  {'JsonName' : jsonMisName,
                        'Latitude' : key['waypoint']['latitudeDeg'],
                        'Longitude' : key['waypoint']['longitudeDeg'],
                        'Speed' : key['waypoint']['speed'],
                        'StepType' : 'waypoint'
                       }
            dfLoc = pd.DataFrame(data = locDict, index = [0])
            dfSteps = pd.concat([dfSteps, dfLoc], ignore_index = True)
            #dfSteps = dfSteps.append(locDict, ignore_index = True)

        if key1 == 'rail':
            locDict = {'JsonName' : jsonMisName,
                       'Bearing' : key['rail']['bearingDeg'], 
                       'Distance' : key['rail']['distance'],
                       'Speed' : key['rail']['speed'],
                       'StepType' : 'rail'
                      }
            dfLoc = pd.DataFrame(data = locDict, index = [0])
            dfSteps = pd.concat([dfSteps, dfLoc], ignore_index = True)
            #dfSteps = dfSteps.append(locDict, ignore_index = True)

        if key1 == 'segment':
            locDict = {'JsonName' : jsonMisName,
                       'Bearing' : key['segment']['bearingDeg'],
                       'Duration' : key['segment']['duration'],
                       'Speed' : key['segment']['speed'],
                       'StepType' : 'segment'
                      }
            dfLoc = pd.DataFrame(data = locDict, index = [0])
            dfSteps = pd.concat([dfSteps, dfLoc], ignore_index = True)
            #dfSteps = dfSteps.append(locDict, ignore_index = True)

    #Completing the dataframe values calculated from locations or distance 

    for idx, step in dfSteps.iterrows() :

        if idx>0 : prevStep = dfSteps.loc[idx-1,:]
        if step.StepType == 'startingPoint':
            pass

        if step.StepType == 'waypoint':

            inverseline = Geodesic.WGS84.InverseLine(prevStep.Latitude, prevStep.Longitude, step.Latitude, step.Longitude, caps=1929)
            #calculate the distance between previous step location to current step location
            dfSteps.loc[idx,'Distance'] = inverseline.s13
            
            #calculate the bearing between the previous step location and the current step location
            dfSteps.loc[idx, 'Bearing'] = inverseline.azi1
            
            #calculate the theoretical step duration from the speed and distance
            dfSteps.loc[idx, 'Duration'] = dfSteps.loc[idx,'Distance'] / step.Speed



        if step.StepType == 'rail':
            dfSteps.loc[idx, 'Duration'] = step.Distance / step.Speed     
            #these 3 lines calculate the lat/long location of the endpoint of the 'rail' step from the previous lat/long locations
            endLoc = distance.distance(meters = step.Distance).destination((prevStep.Latitude, prevStep.Longitude), bearing = step.Bearing)
            dfSteps.loc[idx,'Latitude']= endLoc.latitude
            dfSteps.loc[idx,'Longitude'] = endLoc.longitude

        if step.StepType == 'segment':
            dist = step.Speed * step.Duration
            dfSteps.loc[idx,'Distance'] = dist
            #these 3 lines calculate the lat/long location of the endpoint of the 'rail' step from the previous lat/long locations
            endLoc = distance.distance(meters = dist).destination((prevStep.Latitude, prevStep.Longitude), bearing = step.Bearing)
            dfSteps.loc[idx,'Latitude']= endLoc.latitude
            dfSteps.loc[idx,'Longitude'] = endLoc.longitude
        
    return dfSteps
    
def move_mission(dfR, dfTh, missionName):
    
    # Will move (pan move) the theoretical df (dfTh) in order to fit on the real df (dfR)
    # Must only be used on relative navigation (rails and segment only)
    
    dfCor = dfTh.where(dfTh.JsonName == missionName).dropna(how = 'all')
    
    deltaDfLat = dfR.Latitude.iloc[0] - dfCor.Latitude.iloc[0]

    deltaDfLong = dfR.Longitude.iloc[0] - dfCor.Longitude.iloc[0]

    for idx,row in dfCor.iterrows():
        if row.StepType != 'waypoint':
            dfCor.loc[idx,'Latitude'] = row.Latitude + deltaDfLat
            dfCor.loc[idx,'Longitude'] = row.Longitude + deltaDfLong
        else:
            break
    
    return dfCor

    
    return dfCor

def createDf(csvPath):
    dfRaw = pd.read_csv(csvPath)
    
    # Replace header names with python compatible names
    dfRaw.columns = dfRaw.columns.str.replace(' ','',regex = True)
    dfRaw.columns = dfRaw.columns.str.replace(r"\(.*\)","",regex = True)
    dfRaw.columns = dfRaw.columns.str.replace('+0','',regex = False)
    dfRaw.columns = dfRaw.columns.str.replace('INX','',regex = False)
    
    # Creating a dataframe that only contains the data when the auv is in mission status
    dfMis = dfRaw[dfRaw['AUVStatus'] == 'MISSION']
    dfMis = dfMis[(dfMis['Latitude'] != 0) & (dfMis['Longitude'] != 0)]
    ## Calculating the absolute distance travelled (m) by the AUV with Haversine function

    latRad = np.deg2rad(dfMis['Latitude'])
    longRad = np.deg2rad(dfMis['Longitude'])

    deltaLatRad = latRad.diff()
    deltaLongRad = longRad.diff()

    a = np.sin(deltaLatRad/2)**2 + np.cos(latRad.shift(1)) * np.cos(latRad) * np.sin(deltaLongRad/2)**2
    c = 2 * np.arcsin(np.sqrt(a))
    m = 6378.1E3 * c

    absDistance = m.cumsum() # Absolute distance travelled in meters

    # Adding the absolute distance to dfMis

    dfMis.insert(value = absDistance,
                       column = 'AbsoluteDistance',
                       loc = len(dfMis.columns)
                )

    ##Calculating the bathymetry from altitude and depth
    dfMis.insert(value = dfMis['Altitude']+dfMis['Depth'],
                 column = 'Bathymetry',
                 loc = len(dfMis.columns)
                )
    
    dfMis.insert(value = csvPath.split('/')[-1],
                 column = 'csvName',
                 loc = len(dfMis.columns)
                )
    
    return dfRaw,dfMis

def createDfLeg(dfMis):
    # Creating a dataframe with only the science data from the Legato and localisation
    loc = ['Timesincestartup','Latitude','Longitude','AbsoluteDistance','csvName']
    science = [col for col in dfMis.columns if col.startswith('Legato')]
    dfLeg = dfMis[loc+science]
    dfLeg = dfLeg.dropna()
    dfLeg = dfLeg[dfLeg['Legato3Depth'] > 0.3]
    dfLeg = dfLeg[dfLeg['AbsoluteDistance'] > 10]

    ## Calculating salinities and adding them to dataframes
    # Calculating the Practical Salinity
    SP =  gsw.SP_from_C(dfLeg['Legato3Conductivity'],
                         dfLeg['Legato3Temperature'],
                         dfLeg['Legato3SeaPressure']
                       )

    dfSP = pd.DataFrame(SP, columns = ['PracticalSalinity'])
    dfLeg = pd.concat([dfLeg, dfSP], axis = 1)

    # Calulating the Absolute Salinity (g/kg) (it's normal to have longitude before latitude according to the gsw doc)
    SA = gsw.SA_from_SP(dfLeg['PracticalSalinity'], 
                        dfLeg['Legato3SeaPressure'], 
                        dfLeg['Longitude'], 
                        dfLeg['Latitude']
                       )

    dfSA = pd.DataFrame(SA, columns = ['AbsoluteSalinity'])
    dfLeg = pd.concat([dfLeg, dfSA], axis = 1)


    ## Calculating Conservative Temperature and adding it to dataframes
    # Calculating the Conservative Temperature
    CT = gsw.CT_from_t(dfLeg['AbsoluteSalinity'], 
                       dfLeg['Legato3Temperature'], 
                       dfLeg['Legato3SeaPressure']
                       )

    dfCT = pd.DataFrame(CT,columns = ['ConservativeTemperature'])
    dfLeg = pd.concat([dfLeg, dfCT], axis = 1)


    ## Calculating Density and adding it to dataframes
    # Calculating the Conservative Temperature
    rho =  gsw.rho(dfLeg['AbsoluteSalinity'],
                   dfLeg['ConservativeTemperature'],
                   dfLeg['Legato3SeaPressure']
                       )

    dfRho = pd.DataFrame(rho,columns = ['Density'])
    dfLeg = pd.concat([dfLeg, dfRho], axis = 1)
    
    return dfLeg


class GPXFile:
    def __init__(self):
        self.gpx = gpxpy.gpx.GPX()
    
    def add_waypoints(self, points):
        for idx, point in points.iterrows():
            waypoint = gpxpy.gpx.GPXWaypoint(latitude = point["Latitude_DD"], 
                                             longitude = point["Longitude_DD"],
                                             name = point["Name"])
            self.gpx.waypoints.append(waypoint)
    
    def add_route(self, routeName, points):
        route = gpxpy.gpx.GPXRoute(name = routeName)
        for idx, point in points.iterrows():
            route.points.append(gpxpy.gpx.GPXRoutePoint(point["Latitude"], point["Longitude"], name = str(idx)))
        self.gpx.routes.append(route)
    
    def add_track(self, trackName, points):
        track = gpxpy.gpx.GPXTrack(name = trackName)
        segment = gpxpy.gpx.GPXTrackSegment()
        for idx, point in points.iterrows():
            segment.points.append(gpxpy.gpx.GPXTrackPoint(point["Latitude"], 
                                                          point["Longitude"],
                                                          time = datetime.datetime.fromtimestamp(point['TimestampUTC'])
                                                         ))
        track.segments.append(segment)
        self.gpx.tracks.append(track)
    
    def generate_gpx(self, filePath):
        with open(filePath, 'w') as f:
            f.write(self.gpx.to_xml())
        
        
def batteryUsed(df):
    totalDist = df['AbsoluteDistance'].max()
    batteryUsed = df['BatteryCharge'].max() - df['BatteryCharge'].min()

    print( 'Total distance  traveled (m) : ' + str(totalDist))
    print('Battery used (%) : ' + str(batteryUsed))
    print('Battery rate (%/km) : ' + str(batteryUsed/totalDist*1000))
    
    return totalDist, batteryUsed

def removeTrackOpencpn():
    global opencpnLayerDir
    for f in os.listdir(opencpnLayerDir):
        os.remove(os.path.join(opencpnLayerDir, f))

def deleteOpencpnTracks():
    btn = widgets.interact_manual(removeTrackOpencpn)
    btn.widget.children[0].description = 'Delete OpenCPN tracks'
    btn.widget.children[0].style.button_color = 'LemonChiffon'
    btn.widget.children[0].layout.width = '600px'
    
def opencpnRestart():
    if platform.system() == 'Darwin':
        os.system('pkill -x OpenCPN')
        time.sleep(0.7)
        os.system('open '+ opencpnPath)
    if platform.system() == 'Windows':
        os.system('taskkill /im opencpn.exe')
        time.sleep(3)
        os.popen(opencpnPath)    

def opencpnRestartButton():
    btn = widgets.interact_manual(opencpnRestart)
    btn.widget.children[0].description = 'Restart OpenCPN'
    btn.widget.children[0].style.button_color = 'PeachPuff'
    btn.widget.children[0].layout.width = '600px'


#### Plots (some plots are a bit heavy in the jupyter so I put their def here so they are "hidden")

def plot_3D(dataframe, plottedValue = 'None',graphTitle = '', cbTitle = '', ccs = px.colors.sequential.thermal ,cmap_range = (0,20)): 
    if plottedValue == 'None':
        fig = px.scatter_3d()
    else:
        fig = px.scatter_3d(dataframe, x = 'Latitude', y = 'Longitude', z = 'Legato3Depth', color = plottedValue, 
                            color_continuous_scale=ccs,
                            hover_data=['Timesincestartup'],
                            range_color = cmap_range
                           )
    fig.update_layout(height = 800, width = 1000, title = graphTitle)
    fig.update_layout(scene_dragmode = 'orbit', 
                      scene_camera = dict(up = dict(x=1,y=0,z=0),eye = dict(x=-2,y=0,z=-2)),
                      )

    fig.update_layout(coloraxis_colorbar_title =  cbTitle)


    fig.update_yaxes(
        scaleanchor = "x",
        scaleratio = 1,
      )
    fig.update_traces(marker = dict(size = 2
                                   )
                     )
    return fig
    

def topviewNetcdf(plottedValue, titlee, label, dfLegAllMis, netcdf, dsBathy, dsBathyh0, mask, csvNames):

    cbardf = dfLegAllMis[dfLegAllMis[plottedValue] != 0]
    cbarMin = cbardf[plottedValue].quantile(0.05)
    cbarMax = cbardf[plottedValue].quantile(0.95)

    fig = go.Figure()
    fig.update_layout(height = 800, width = 900,title = titlee)
    fig.update_xaxes(showgrid =False)
    fig.update_yaxes(showgrid =False)
    fig.layout.plot_bgcolor = '#cce0f0'

    showscaleFlag = True

    if netcdf != '':
        bathy = go.Contour(z = dsBathyh0*mask,
                           x = dsBathy.variables['longitude'][0,:],
                           y =dsBathy.variables['latitude'][:,0],
                           name = 'Bathy',
                           colorscale = 'greys', #greys
                           showscale = False,
                           contours=dict(
                               start=-5,
                               end=20,
                               size=2,
                               #coloring='lines',
                               #showlabels = True
                           )
                          )
        fig.add_trace(bathy)



    for csv in csvNames:
        csvOnly = csv.split('/')[2]
        df = dfLegAllMis.where(dfLegAllMis.csvName == csvOnly).dropna(how = 'all')
        
        if (df[plottedValue] == 0).all():
            continue

        trace = go.Scatter(x = df.Longitude, y = df.Latitude,
                           hovertext=df[plottedValue],
                           mode = 'markers',
                           name = 'MIS' + csv[-11:-4],
                           marker = dict(showscale = showscaleFlag,
                                         size = 1,
                                         color = df[plottedValue],
                                         cmin = cbarMin,
                                         cmax = cbarMax,
                                         colorbar = dict(len = 1,
                                                         orientation = 'h',
                                                         title = label)
                                        )
                                ) 

        showscaleFlag = False

        fig.add_trace(trace)

    return fig
    
    
def topviewOPenstreetmap(plottedValue, titlee, label, dfLegAllMis, csvNames):
    
    fig = go.Figure()
    fig.update_layout(height = 800, width = 800, title = titlee)
    fig.update_layout(mapbox = dict(style = "open-street-map",
                                    zoom = 11,
                                    center = dict(lat = dfLegAllMis.Latitude.mean(), 
                                                  lon = dfLegAllMis.Longitude.mean())
                                   )              
                     )

    cbardf = dfLegAllMis[dfLegAllMis[plottedValue] != 0]
    cbarMin = cbardf[plottedValue].quantile(0.05)
    cbarMax = cbardf[plottedValue].quantile(0.95)

    showscaleFlag = True


    for csv in csvNames:
        csvOnly = csv.split('/')[2]
        df = dfLegAllMis.where(dfLegAllMis.csvName == csvOnly).dropna(how = 'all')
        
        if (df[plottedValue] == 0).all():
            continue

        trace = go.Scattermapbox(lon = df.Longitude, 
                                 lat = df.Latitude,
                                 hovertext=df[plottedValue],
                                 name = 'MIS' + csv[-11:-4],
                                 mode='markers',
                                 marker=go.scattermapbox.Marker(
                                    size=3,

                                    color=df[plottedValue],
                                    cmin = cbarMin,
                                    cmax = cbarMax,
                                    showscale = showscaleFlag,
                                    colorbar = dict(len = 1,
                                                         orientation = 'h',
                                                         title = label)

                                    )
                                )
        showscaleFlag = False 

        fig.add_trace(trace)

    return fig

def topviewMatplotlib(dataframe, dsBathy, dsBathyh0,csvNames, plottedValue = '', cbTitle = '', plotTitle = '', colormap = 'plasma'):
    data_crs = ccrs.PlateCarree()

    fig=plt.figure(figsize=(14,9)) 
    fig.set_facecolor('w')
    ax = fig.add_subplot(1, 1, 1, projection=data_crs)

    extent_offset = .03
    extent = [dataframe.Longitude.min()-extent_offset, dataframe.Longitude.max()+extent_offset, 
              dataframe.Latitude.min()-extent_offset, dataframe.Latitude.max()+extent_offset]

    ax.set_extent(extent, data_crs)


    ax.add_feature(cfeature.GSHHSFeature('f', edgecolor='black', facecolor='beige'))
    gl = ax.gridlines(draw_labels=True) 
    gl.top_labels, gl.right_labels = False, False
    gl.xlabel_style, gl.ylabel_style = {'fontsize': 12}, {'fontsize': 12}
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    #if bathyType == 'MARS':
    #    h0 = dsBathy.variables['H0'][:,:] # MARS Bathy
    #if bathyType == 'CROCO':
    #    h0 = dsBathy.variables['h'][:,:] # CROCO Bathy
    
    dsBathyh0=np.ma.filled(dsBathyh0,fill_value=0.)

    lon = dsBathy.variables['longitude'][:,:]
    lat = dsBathy.variables['latitude'][:,:]


    m = ax.contourf(lon,lat,dsBathyh0,levels=np.arange(0,30,0.1),cmap="Greys",transform=data_crs)


    for csv in csvNames:    
        csvOnly = csv.split('/')[2]
        df = dataframe.where(dataframe.csvName == csvOnly).dropna(how = 'all')
        l=ax.scatter(df.Longitude,df.Latitude,s=5,c=df[plottedValue],
                     vmin=dataframe[plottedValue].quantile(0.05),vmax=dataframe[plottedValue].quantile(0.95),
                     cmap = colormap
                    )

    cb = fig.colorbar(l,label=cbTitle,shrink=0.5)


    cb_titre = 'Depth (m)'
    cb = fig.colorbar(m,label=cb_titre,orientation='horizontal',shrink=0.5)

    plt.title(plotTitle)
    
    return plt